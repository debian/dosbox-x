Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dosbox-x
Source: https://github.com/joncampbell123/dosbox-x
Files-Excluded:
 .github
 .vscode
 build-scripts
 contrib/fonts/*.ttf
 contrib/fonts/FREECG98_OLD.BMP
 contrib/mappings/*.TXT*
 contrib/windows
 docs/B*
 docs/G*
 docs/I*
 docs/PLANS/S*
 docs/Ref-FM-Towns-Bochs
 docs/RISC
 docs/Windows*
 include/crypt.h
 include/fluidsynth
 include/fluidsynth.h
 include/gmock
 include/gtest
 include/ioapi.h
 include/iowin32.h
 include/mztools.h
 include/ogg
 include/opus*.h
 include/resource.h
 include/sdk2_glide.h
 include/speex
 include/unzip.h
 include/zip.h
 NOTES/MCGA*
 NOTES/PC-98*
 optimization-1
 patch-integration
 pc98-testme-1
 ref
 ROMs
 snapshots
 src/builtin/*_exe.cpp
 src/builtin/*_exe_pc*.cpp
 src/builtin/4dos.cpp
 src/builtin/4DOS_img.h
 src/builtin/buffers_com.cpp
 src/builtin/cdplay.*
 src/builtin/device_com.cpp
 src/builtin/dosmid.*
 src/builtin/edit_com.cpp
 src/builtin/emsmagic.*
 src/builtin/eval.*
 src/builtin/fcbs_com.cpp
 src/builtin/files_com.cpp
 src/builtin/glide2x.*
 src/builtin/lastdriv_com.cpp
 src/builtin/mem_com.cpp
 src/builtin/move_exe.cpp
 src/builtin/mpxplay.*
 src/builtin/ne2000bin.*
 src/builtin/print_com.cpp
 src/builtin/shutdown.*
 src/builtin/sys_com.cpp
 src/builtin/zip.*
 src/builtin/xcopy
 src/libs/decoders/internal
 src/libs/decoders/xxhash.h
 src/libs/fluidsynth
 src/libs/libchdr/zstd
 src/libs/physfs
 src/libs/zmbv/unused-source-files.zip
 src/resource.h
 tests/gmock
 tests/gtest
 vs
Files-Included:
 contrib/windows/installer/drivez_readme.txt
 vs/sdl/src/cdrom/compat_SDL_cdrom.h
 vs/sdl/src/cdrom/linux/SDL_syscdrom.c
 vs/sdl/src/cdrom/SDL_syscdrom.h
 vs/sdl/src/cdrom/SDL_cdrom.c

Files: *
Copyright: 2002-2021 The DOSBox Team
           2020-2021 The DOSBox Staging Team
           2011-2022 The DOSBox-X Team
           2011-2025 Jonathan Campbell (Castus)
License: GPL-2.0-or-later

Files: contrib/fonts/wqy_1?pt.bdf
Copyright: 2004-2006, The WenQuanYi Project
License: GPL-2.0 with font exception
 As a special exception, if you create a document which uses this
 font, and embed this font or unaltered portions of this font into the
 document, this font does not by itself cause the resulting document
 to be covered by the GNU General Public License. This exception does
 not however invalidate any other reasons why the document might be
 covered by the GNU General Public License. If you modify this font,
 you may extend this exception to your version of the font, but you
 are not obligated to do so. If you do not wish to do so, delete this
 exception statement from your version.

Files: contrib/glshaders/crt-caligari.glsl
Copyright: 2011 caligari
License: GPL-2.0-or-later

Files:
 contrib/glshaders/crt-aperture.glsl
 contrib/glshaders/crt-easymode*.glsl
Copyright: EasyMode
License: GPL-2.0-only

Files: contrib/glshaders/crt-geom*.glsl
Copyright: cgwg, Themaister and DOLLS
License: GPL-2.0-or-later

Files: contrib/glshaders/crt-lottes*.glsl
Copyright: N/A
License: public-domain
 PUBLIC DOMAIN CRT STYLED SCAN-LINE SHADER
 by Timothy Lottes

Files: contrib/glshaders/crt-pi.glsl
Copyright: 2015-2016 davej
License: GPL-2.0-or-later

Files: contrib/glshaders/pixellate.glsl
Copyright: 2011, 2012 Fes
License: MIT

Files:
 contrib/glshaders/crt-hyllian.glsl
 contrib/glshaders/xbr-lv*.glsl
Copyright: 2011-2016 Hyllian
License: MIT

Files: contrib/glshaders/zfast_crt.glsl
Copyright: 2017 Greg Hogan (SoltanGris42)
License: GPL-2.0-or-later

Files: debian/*
Copyright: 2022-2025 Stephen Kitt <skitt@debian.org>
License: GPL-2.0-or-later

Files: include/DOSBoxTTF.h
Copyright: Steve Matteson
           2010 Google Corporation
Comment: the DOSBoxTTFbi data structure encodes Cousine-vDos.
License: Apache-2.0

Files: include/enet.h
Copyright: 2002-2016 Lee Salzman
           2017-2021 Vladyslav Hrytsenko, Dominik Madarász
License: MIT

Files: include/fpu_control_x86.h
Copyright: 1993, 1995-1998, 2000-2002 Free Software Foundation, Inc.
License: LGPL-2.1-or-later

Files:
 include/ntddcdrm.h
 include/ntddscsi.h
Copyright: N/A
License: public-domain
 THIS SOFTWARE IS NOT COPYRIGHTED
 .
 This source code is offered for use in the public domain. You may
 use, modify or distribute it freely.
 .
 This code is distributed in the hope that it will be useful but
 WITHOUT ANY WARRANTY. ALL WARRANTIES, EXPRESS OR IMPLIED ARE HEREBY
 DISCLAIMED. This includes but is not limited to warranties of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Files:
 include/qcow2_disk.h
 src/ints/qcow2_disk.cpp
Copyright: 2016 Michael Greger
License: GPL-2.0-or-later

Files:
 include/ne2000.h
 src/hardware/ne2000.cpp
Copyright: 2001-2002 MandrakeSoft S.A.
License: GPL-2.0-or-later

Files:
 include/SDL_ttf.h
 src/gamelink/scancodes_windows.h
 src/gui/sdl_ttf.c
Copyright: 2001-2019 Sam Lantinga
License: zlib-acknowledgement
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

Files:
 include/whereami.h
 src/gui/whereami.c
Copyright: Gregory Pakosz
License: WTFPL or MIT

Files:
 include/zipcrc.h
 src/gui/zipcrc.c
Copyright: N/A
Comment: See https://pycrc.org/faq.html
License: pycrc
 Who owns the copyright of the generated code?
 .
 The sloppy answer is: you are free to do whatever you like with the
 generated code, as long as you don't blame the author for any
 malfunction or damage caused by the program.
 .
 However, as a courtesy, please keep the line that states that the
 code was generated by pycrc.

Files: src/debug/debug_disasm.cpp
Comment:
 The shipped dosbox-x binary doesn't use this. It's kept in the source
 for the debug build which is used to run the tests, during the build.
 Determining the license of this file requires exploring its lineage.
 The reference to DJ Delorie and Kent Williams points to
 src/debug/edbug/unassmbl.c in DJLSR200.ZIP, which is covered by
 GPL-2.0-only. Later releases of  DJGPP never changed this as far as I
 can determine.
Copyright: DJ Delorie and Kent Williams
           Robin Hilliard
           Andrea Mazzoleni
License: GPL-2.0-only

Files: src/dos/crc32.h
Copyright: 1986 Gary S. Brown
License: no-restriction
 You may use this program, or code or tables extracted from it, as
 desired without restriction.

Files: src/dos/dos_codepages.h
Copyright: 1993-1996 Kosta Kostis
           1996-2002 Markus Oberhumer & Laszlo Molnar
           Henrique Peron
License: GPL-2.0-or-later

Files: src/dos/dos_keyboard_layout_data.h
Copyright: 2004 Aitor SANTAMARIA_MERINO
           Henrique Peron
License: GPL-2.0-or-later

Files: src/dos/dos_programs.cpp
Copyright: 2002-2021 The DOSBox Team
           2020 the DOSBox Staging Team
           Pogoman361 and Wengier
           sduensin and Wengier
License: GPL-2.0-or-later

Files: src/hardware/esfmu/*
Copyright: 2023 Kagamiin~
License: LGPL-2.1-or-later

Files: src/hardware/imfc*
Copyright: 1997-1999 Jarek Burczynski
           1999-2000 Daisuke Nagano
           2017-2020 Loris Chiocca
License: GPL-2.0-or-later

Files: src/hardware/mame/fmopl.*
Copyright: Jarek Burczynski, Tatsuyuki Satoh
License: GPL-2.0-or-later

Files: src/hardware/mame/saa1099.*
Copyright: Juergen Buchmueller, Manuel Abadia
License: BSD-3-Clause

Files: src/hardware/mame/sn76496.*
Copyright: Nicola Salmoria
License: BSD-3-Clause

Files:
 src/hardware/mame/ymdeltat.*
 src/hardware/mame/ymf262.*
Copyright: Jarek Burczynski
License: GPL-2.0-or-later

Files: src/hardware/nukedopl.*
Copyright: 2013-2020 Nuke.YKT
License: LGPL-2.1-or-later

Files: src/hardware/opl.*
Copyright: 1998-2001 Ken Silverman
           2002-2021 The DOSBox Team
License: LGPL-2.1-or-later

Files: src/hardware/reSID/*
Copyright: 1999, 2004 Dag Lem
License: GPL-2.0-or-later

Files: src/hardware/RetroWaveLib/*
Copyright: 2021 ReimuNotMoe
License: AGPL-3.0-or-later

Files:
 src/hardware/voodoo_data.h
 src/hardware/voodoo_def.h
 src/hardware/voodoo_emu.cpp
 src/hardware/voodoo_types.h
 src/libs/libchdr/*
Copyright: Aaron Giles
License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in
       the documentation and/or other materials provided with the
       distribution.
     * Neither the name 'MAME' nor the names of its contributors may be
       used to endorse or promote products derived from this software
       without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY AARON GILES ''AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL AARON GILES BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files:
 src/ints/bios_memdisk.cpp
 src/ints/bios_vhd.cpp
Copyright: 2018 Shane Krueger
           2023 maxpat78
License: GPL-2.0-or-later

Files: src/ints/int_dosv.cpp
Copyright: 2019 takapyu
           2021 The DOSBox-X Team
License: GPL-2.0-or-later

Files: src/libs/decoders/archive.h
Copyright: N/A
License: Unlicense

Files: src/libs/decoders/audio_convert.c
Copyright: 1997-2001 Sam Lantinga
License: LGPL-2.0-or-later
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,
 USA.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2 can be found in /usr/share/common-licenses/LGPL-2

Files:
 src/libs/decoders/dr_flac.h
 src/libs/decoders/dr_mp3.h
 src/libs/decoders/dr_wav.h
Copyright: 2020 David Reid
License: Unlicense or MIT-0

Files:
 src/libs/decoders/flac.c
 src/libs/decoders/mp3.cpp
 src/libs/decoders/wav.c
Copyright: 2001-2017 Ryan C. Gordon
           2018-2019 Kevin R. Croft
           2020-2021 The DOSBox Staging Team
License: GPL-2.0-or-later

Files: src/libs/decoders/mp3_seek_table.*
Copyright: 2018-2019 Kevin R. Croft
           2020 The DOSBox Staging Team
License: GPL-2.0-or-later

Files: src/libs/decoders/opus.c
Copyright: 2019 Kevin R Croft
License: GPL-3.0-or-later
 This DOSBox Ogg Opus decoder backend is free software: you can redistribute
 it and/or modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This DOSBox Ogg Opus decoder backend is distributed in the hope that it
 will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 along with DOSBox.  If not, see <http://www.gnu.org/licenses/>.

Files: src/libs/decoders/SDL_sound*
Copyright: 2001 Ryan C. Gordon
License: LGPL-2.1-or-later

Files: src/libs/decoders/stb*.h
Copyright: 2017 Sean Barrett
Comment: This is packaged in libstb-dev but DOSBox-X doesn't build
 with the Debian-packaged version.
License: MIT or Unlicense

Files: src/libs/decoders/vorbis.c
Copyright: 2001-2017 Ryan C. Gordon
           2018-2021 The DOSBox Staging Team
License: GPL-2.0-or-later

Files: src/libs/gui_tk/*
Copyright: 2005-2013 Jörg Walter
License: GPL-3.0-or-later

Files: src/libs/libchdr/lzma/*
Copyright: N/A
License: public-domain
 Igor Pavlov: Public domain

Files: src/libs/mt32/*
Copyright: 2003-2006, 2008-2009 Dean Beeler, Jerome Fisher
           2011-2022 Dean Beeler, Jerome Fisher, Sergey V. Mikayev
License: LGPL-2.1-or-later
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2.1 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in /usr/share/common-licenses/LGPL-2.1

Files: src/libs/mt32/sha1/sha1.*
Copyright: 2011 Micael Hildenborg
License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Micael Hildenborg nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY Micael Hildenborg ''AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Micael Hildenborg BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: src/libs/passthroughio/*
Copyright: 2023-2024 Daniël Hörchner
License: GPL-2.0-or-later

Files: src/libs/tinyfiledialogs/*
Copyright: 2014-2023 Guillaume Vareille
License: zlib-acknowledgement

Files: src/libs/xBRZ/*
Copyright: Zenju
License: GPL-3.0-only
 This file is part of the xBRZ project. It is distributed under
 GNU General Public License: https://www.gnu.org/licenses/gpl-3.0

Files: src/misc/winiconv.c
Copyright: N/A
License: public-domain
 This file is placed in the public domain.

Files: src/output/direct3d/direct3d.*
Copyright: gulikoza
License: GPL-2.0-or-later

Files: src/output/direct3d/hq2x_d3d.*
Copyright: Mitja Gros
           2004-2005 Jorg Walter
License: GPL-2.0-or-later

Files: src/output/direct3d/ScalingEffect.*
Copyright: 2003 Ryan A. Nunn
License: GPL-2.0-or-later

Files: src/output/ppscale.*
Copyright: 2018-2020 Anton Shepelev
Comment: See https://launchpad.net/ppscale for the license link
License: Fair
 Usage of the works is permitted  provided that this instrument is retained
 with the works, so that any entity that uses the works is notified of this
 instrument.    DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

Files: vs/sdl/src/cdrom/*
Copyright: 1997-2012 Sam Lantinga
License: LGPL-2.1-or-later
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in /usr/share/common-licenses/LGPL-2.1


License: AGPL-3.0-or-later
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 .
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
    http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License version 2
 can be found in /usr/share/common-licenses/Apache-2.0

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the <organization> nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2.0-only
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2 as
 published by the Free Software Foundation.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in /usr/share/common-licenses/GPL-2

License: GPL-2.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,
 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in /usr/share/common-licenses/GPL-2

License: GPL-3.0-or-later
 This file is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in /usr/share/common-licenses/GPL-3

License: LGPL-2.1-or-later
 The GNU C Library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 The GNU C Library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in /usr/share/common-licenses/LGPL-2.1

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice (including the next
 paragraph) shall be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

License: MIT-0
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Unlicense
 This is free and unencumbered software released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile, sell, or
 distribute this software, either in source code form or as a compiled
 binary, for any purpose, commercial or non-commercial, and by any
 means.
 .
 In jurisdictions that recognize copyright laws, the author or authors
 of this software dedicate any and all copyright interest in the
 software to the public domain. We make this dedication for the
 benefit of the public at large and to the detriment of our heirs and
 successors. We intend this dedication to be an overt act of
 relinquishment in perpetuity of all present and future rights to this
 software under copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 For more information, please refer to <http://unlicense.org/>

License: WTFPL
 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 
 .
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 .
 0. You just DO WHAT THE FUCK YOU WANT TO.

License: zlib-acknowledgement
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software.  If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

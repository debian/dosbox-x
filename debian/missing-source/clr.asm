	[BITS 16]
	[ORG 0x100]

	[SECTION .text]

start:	mov ah, 0x0f		; Get current video mode
	int 0x10
	and al, 0x7f		; Clear the "no blanking" bit
	cmp al, 0x03		; Colour text mode
	jbe text
	cmp al, 0x07		; B&W text mode
	je text
	cmp al, 0x54		; 132-column text modes (some)
	je text
	cmp al, 0x55
	je text
	mov ah, 0		; Set video mode
	jmp exit
text:	mov ah, 0x06		; Scroll up window
	mov al, 0		; Clear entire window
	mov bh, 0x07		; Light gray on black
	xor cx, cx		; Top-left of screen
	mov ds, cx
	mov dh, [0x0484]	; Rows on screen minus one
	mov dl, [0x044a]	; Columns on screen
	dec dl
	int 0x10
	mov ah, 0x02		; Set cursor position
	mov bh, 0		; Page number 0
	xor dx, dx		; Top-left of screen
exit:	int 0x10
	int 0x20		; Exit

	[BITS 16]
	[ORG 0x100]

	[SECTION .text]

start:	mov ax, 0x1202		; Alternate function select, 400 scan lines
	mov bl, 0x30		; Select vertical resolution
	int 0x10
	mov ax, 0x0003		; Set video mode, text 80x25
	int 0x10
	int 0x20		; Exit
